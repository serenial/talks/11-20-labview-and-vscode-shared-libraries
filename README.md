This repository explains the process and provides example code so you can get setup with developing shared libraries for LabVIEW using VSCode.

The code in the `src` directory is compatible with LabVIEW 2018 but the VSCode setup assumes you are using LabVIEW 2020 (32-bit). If you are using an earlier version of LabVIEW then hopefully modifying the VSCode build/configuration settings won't be too difficult.

This setup also assumes that you are developing for Windows but much of it is true for developing for Mac OSX and Linux distributions.


[[_TOC_]]

## Graphical programming is the best - why are we messing with the rest?
* Sometimes you _have_ to use hardware that doesn't come with LabVIEW drivers 
* You might have to integrate current non LabVIEW code where re-implementing it in LabVIEW isn't feasible (time/complexity/performance...)
* You get access to a wide range of open source libraries - most languages can export C-language style function calls or if you are interested in Python then checkout [Danielle Jobe's excellent talk on using the Python nodes](https://www.youtube.com/watch?v=0rzWCa-mDoo&t=19m55s) (LV 2018 onwards) from WUELUG November 2020.

## Alas - It's not all plain sailing
* You may have to setup and maintain a new development and build environment
* Each operating system (Windows/Mac/Linux) and system bitness (32/64) requires different shared libraries
* You have additional dependencies to think about when distributing your application/code
* You will crash LabVIEW a lot when getting started - but you will get better!


# How do we call shared library functions in LabVIEW?
Shared libraries define exported functions which LabVIEW calls via the `Call Library Function` node (_Connectivity>>Libraries and Executables in the palettes_). 

This node allows us to pass input data to an external-library function and receive the returned value - much like calling any subVI.

![Call Library Function](<img/palette.png?raw=true>)

## Setting up the Call Library Function node

We are going to be using `example.dll` which exports a C-language style function `add_one` which adds one to an integer and returns the result. The function call is:
```c
int add_one(int a)
```

To use this function in LabVIEW, first drop a  `Call Library Function` node on the block diagram and double-click to bring up the configuration dialog;
 
![Call Library Function Configuration - Function Call Configuration](<img/CLF-function.png?raw=true>)

### Call Library Function Configuration
The LabVIEW Help article [Configuring the Call Library Function Node](https://zone.ni.com/reference/en-XX/help/371361R-01/lvexcodeconcepts/configuring_the_clf_node/) gives comprehensive information on setting this node up but the settings we will use are;

**Firstly the `Function` tab;**

#### _Library name or path_
This is where we point LabVIEW to the external library binary file that contains the function we want to call. We also have the option of specifying the library path on the block diagram. There are some key advantages and disadvantages to each option but for the time being we will specify just library using the file chooser dialog to the dll that matches your version of LabVIEW's bitness in the build directory of this repository. If you want to make your code more portable then change the name from `example32.dll` or `example64.dll` to `example*.dll` and LabVIEW will automatically choose the correct 32 or 64 bit dll.


#### _Function Name_
When we specify the path to a library, LabVIEW reads the available functions so we should be able to easily select `add_one` from the function name dropdown menu.


#### _Run in UI Thread/Any Thread_
In some cases, a shared library will store data internally between calls to it - a bit like a LabVIEW functional global. Specifying to run in the UI thread means that the shared library's internal data will be consistent between calls but it comes with a performance hit as LabVIEW code has to wait for the UI thread to become available.

Of course some functions will be pure - their output solely depends on their input then we can safely call the function from any thread.

For our `add_one` function we can select `Run in any thread`.


#### _Calling Convention_
The calling convention determines how the function is expecting to be passed data and how data is cleaned up afterwards. The `example32.dll` uses the C declaration standard as lots of code probably will.


**Now the `Parameters` tab;**

Here we can specify the types and order of the return and input variables so we need to do the following;
* Change the return type from `void` to a `Numeric` of `Signed 32-bit Integer` type.
* Add an input parameter which is `Numeric` of `Signed 32-bit Integer` type.


![Call Library Function Configuration - Parameter Configuration](<img/CLF-param.png?raw=true>)


You can check that the Function Prototype below the parameter list appears as we are expecting.

**Next the `Callbacks` tab;**
We don't need labVIEW to call any functions when it loads or unloads the shared library so we keep this as it is.

**Finally the `Error Checking` tab;**
We can set this to maximum whilst we are getting started as it will help catch some errors.

## Testing our first Call Library Function

With the `Call Library Function` node configured we need to create controls for input `a` and indicators for the error out and `result` terminal. Then save the VI, set some input values and run. You should have successfully added one to an integer!


![Add.vi - Add one to a number using a Call Library Function node](<img/add.png?raw=true>)


# Creating a Shared Library
Suppose we decided that we didn't just want to add one to an integer but perform a range of mathematical operations - How could we create our own shared library?

On Windows we might reach for the mighty Visual Studio but this tool can be a bit much for writing and building a simple dll.

Instead we are going to see how we can setup [Visual Studio *Code*](https://code.visualstudio.com/) (the open-source, cross-platform integrated development environment from Microsoft).

VScode has great support for developing in a range of languages and is highly customizable.

![Visual Studio Code](<img/VScode.png?raw=true>)

## Installing Dependencies

We will also need the following:
* [The C/C++ extention for VSCode](https://code.visualstudio.com/docs/languages/cpp) which is installed from within VSCode's extension browser.
* [Build Tools for Visual Studio 20xx](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019) (unless we have the full Visual Studio also installed). This provides the Microsoft Visual C compiler and linker tools. It can be tricky to find on the Microsoft page, so scroll down to the _Tools For Visual Studio_ section

![Microsoft Build Tools](<img/Build-tools.png?raw=true>)


## Create a C-language file
We aren't going to bother with a header file nor do we have to define the DllMain entry point. We just need a file called `simple-shared-library.c` in the `src` directory with the following:

```c
__declspec(dllexport) int add(int a, int b){
    return a + b;
}
```

the `__declspec(dllexport)` declaration means that the function will be exported when we build a dll.

## Setup a VSCode Build Task
A build task will allow us to easily build our dll with one click.
To set it up we perform the following:
* Ensure your `simple-shared-library.c` has focus in the main text window of VSCode and from the top menu select _Terminal>>Configure Default Build Task..._
* In the window that appears, select `C/C++ cl.exe - Build Active File`. This creates a `tasks.json` file in a `.vscode` directory
* Create a `build` directory (if it doesn't already exist) as the location where the build output will be placed
* Modify the `task.json` file so it builds our chosen source into a dll as follows;

```javascript
{
	"version": "2.0.0",
	"tasks": [
		{
			"type": "shell", // run this from anywhere in project without a c/c++ file having focus
			"label": "C/C++: cl.exe build simple-shared-library", //change the task name to something meaningful
			"command": "cl.exe",
			"args": [
				"/Zi",
				"/EHsc",
				"/LD", // this flag means output a dll not an executable
				"/Fe:",
				"build\\simple-shared-library.dll", // change build location and file to .dll
				"src\\c\\simple-shared-library.c" // change source path
			],
			"options": {
				"cwd": "${workspaceFolder}"
			},
			"problemMatcher": [
				"$msCompile"
			],
			"group": {
				"kind": "build",
				"isDefault": true
			},
			"detail": "Build simple-shared-library dll"
		}
	]
}
```

## Build the DLL
Use the shortcut `ctrl`-`shift`-`b` to build or select _Terminal>>Run Build Task_ from the VSCode menu. This will open a terminal window and you will probably see this:
```ps
Starting build...
Build finished with error:
'cl.exe' is not recognized as an internal or external command,
operable program or batch file.

The terminal process failed to launch (exit code: -1).
```
## Relaunching VSCode with the Visual Studio Build Tools In Place
What went wrong? Our beautiful build task! 

Unfortunately the default windows environment does not include the system variables for building using the visual studio build tools.

The easiest option is to close VSCode and relaunch it from either the `x86 Native Tools Command Prompt for VS 20xx` if you are using 32-bit LabVIEW or `x64 Native Tools Command Prompt for VS 20xx` if you are using 64-bit LabVIEW. 

To do this, use the Window's start menu and start typing the name of the command prompt you want, launch it and then type `code` to start VSCode with all the build tools in place.

![Launching the developer command prompt](<img/command-prompt.png?raw=true>)

## Building the DLL (Take 2)
Repeat the steps to run the build task and check that the DLL builds successfully

```ps
Microsoft (R) C/C++ Optimizing Compiler Version 19.27.29111 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

simple-shared-library.c
Microsoft (R) Incremental Linker Version 14.27.29111.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/debug
/dll
/implib:build\simple-shared-library.lib
/out:build\simple-shared-library.dll 
simple-shared-library.obj
   Creating library build\simple-shared-library.lib and object build\simple-shared-library.exp

Terminal will be reused by tasks, press any key to close it.
```
## Calling the Add function in LabVIEW
Repeat the steps from the begining to make a call to `add` from LabVIEW from the new `simple-shared-library.dll` and save this as `add.vi`.

# Debugging Code with a Shared Library
We can also use VSCode to debug LabVIEW VIs which call shared libraries if we built the library with debugging symbols. As this is the default configuration from the previous build step we should see a `simple-shared-library.pdb` alongside the dll.

To setup the debug configuration we need to create a `launch.json` file in the .vscode directory with the following:
```javascript
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "(Windows) Attach",
            "type": "cppvsdbg",
            "request": "attach",
            "processId": "${command:pickProcess}"
        }
    ]
}
```

## Setting a Breakpoint in the C-code
Open the `simple-shared-library.c` file and click just to the left of the row number for the `return` statement. This creates a breakpoint which will mean that when we are in debugging mode in VSCode, the code execution will be paused and we can inspect the current state of the application.

![Addition of a breakpoint in the add function](<img/breakpoint.png?raw=true>)

## Running with Debugging
Open the `add.vi` in LabVIEW but do not run it. Now in VSCode select _Run>>Start Debugging_ from the top menu. This will open a small dialog asking which process to attach to. Start typing _LabVIEW_ to find LabVIEW.exe and press enter. This should open the debug console at the bottom of the VSCode window.

Now run `add.vi` and observe how focus swaps to VSCode with the execution paused at the breakpoint.

The debugging flow can be controlled by the run/step into/step over icons and the variables and stack are visible on the left hand side by selecting the run sidebar from the activity bar.

![Debugging in VSCode](<img/Debug-Tools.png?raw=true>)

## Avoiding Linking Errors During Development
Taking another look at the Call Library Function node - if we specify the library path in the configuration then LabVIEW will load the shared library binary when it opens the VI and only release it when all VIs which call that library are closed. This can slow development as if we try to build our shared library binary whilst LabVIEW has it loaded we will see the following build error during linking:

```ps
LINK : fatal error LNK1168: cannot open build\simple-shared-library.dll for writing
The terminal process "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -Command cl.exe /Zi /EHsc /LD /Fe: build\simple-shared-library.dll src\simple-shared-library.c" terminated with exit code: 1.
```
To avoid this, we use the fact that we can force LabVIEW to unload the shared library binary by configuring the Call Library Function node so that the library path is specified on the block diagram. If we want to unload the library, we can supply the Call Library Function node with an empty path and it will unload the shared library binary!

This sounds great but there is on caveat; We have to use the exact same node to unload the library so a structure like the one below is required.

![Calling and unloading the shared library binary](<img/call-and-unload.png?raw=true>)

Here we use a for loop which will execute the contained code twice.
The first time, the shared library binary path is supplied and the output of the Call Library Function node are captured to the error-out and result indicator.
On the second call, an empty path will be passed into the Call Library Function node and it will unload the shared library binary.

Note that loading and unloading the shared library binary on each call is not advisable for real-life code due to the performance hit so be considerate of when and where to use this technique.

# Tighter Integration with LabVIEW
Often if we are integrating shared libraries with LabVIEW we will find ourselves unable to take the shared library as-is. So we have to write a wrapper in the same language as the shared library which bundles a number of complex function calls, often dealing with non-LabVIEW compatible types, into some easier-to-call high level functions. If this is the case then we might consider writing a wrapper which is more tightly integrated with LabVIEW so we can more easily interact with objects like labVIEW Arrays and Strings.

LabVIEW provides a library (labview.lib on windows) and a number of C-language headers, which give access to LabVIEW's _Memory Manager_ functions, some file I/O tools and a few other utilities.

When we include code from this Library we need to tell our build tools where to find the necessary files for both building and linking of the shared library binary. These files are all stored alongside the LabVIEW.exe in a directory called `cintools`. To utilize these functions you need to add additional flags to the compilation instruction in our build task (in `task.json`).

## Adding to the Build Script

```javascript
	"args": [
		"/Zi",
		"/LD",
		"/EHsc",
		"/I", // include
		"C:\\Program Files (x86)\\National Instruments\\LabVIEW 2020\\cintools", // directory with extcode.h
		"/Fe:",
		"build\\<LIBRARY-OUTPUT-NAME>.dll",
		"src\\<INPUT-FILE-NAME>.c",
		"/link", //specify the location of labview.lib to the linker
		"/LIBPATH",
		"C:\\Program Files (x86)\\National Instruments\\LabVIEW 2020\\cintools\\labview.lib", // link with library labview.lib
		"user32.lib" // required library for DbgPrintf
	]
}
```

## Configuring VSCode's C/C++ Extension
To enable VSCode's intellisense then we need to configure the include paths in the C/C++ extension. This is easily achieved by selecting _View>>Command Palette_ in VSCode's top menu and typing C/C++: Edit Configuration (UI) and adding the path to the `cintools` directory;

![Extension Configuration](<img/extension-configuration.png?raw=true>)

## A Warning on Tooling!
The LabVIEW C-Integration library only supports Microsoft Visual Build Tools on Windows (not `gcc` via cygwin etc).

## Memory Manager Functions
The [LabVIEW Memory Manager functions](https://zone.ni.com/reference/en-XX/help/371361R-01/lvexcode/memory_manager_functions/) allow us to adjust the size of arrays and strings we have been passed handles to within our shared library code. Without these we normally need to somehow pre-allocate memory in LabVIEW for the result of our shared library function to write data to.

If we had a function called `get_message` which returned some useful information but of an unknown length then we can leverage the memory manager function `DSSetHandleSize`. This takes a _string handle_ which is a reference to a memory location that LabVIEW is managing and allows us to specify the size we need it to be to write our message into.

### **Update (October 2024)**
Just before the code - I thought I would jump in to flag that you need to take care when determining the size of an array or string handle as the exact number of bytes differs depending on if you are on 32 or 64 bit systems.

On x64 systems there will be 4-bytes of padding between the `cnt` value and the data pointer.

The easiest way to handle this is to use the `NumericArrayResize` Memory Manager function to allocate and size a handle as this accounts for platform dependent padding.

Alternatively the number of bytes to allocate for a string or array with `DSSetHandleSize` should be `sizeof(LStr) + message_length -1` - this is because the `sizeof` for a `LStr` is the size of a 1 character string so you just need to add the rest of the length of the string to get the total number of bytes.

```c
// include LabVIEW C-Integration Library Header

#include "extcode.h"

__declspec(dllexport) void get_message(LStrHandle output){

	char message [] = "hello";
    int message_length = strlen(message);

    // LabVIEW Strings are stored as a struct of the form:
    // {
	//        int32	cnt;		/* number of bytes that follow */
	//        uChar	str[1];		/* cnt bytes */
    // } 
    
    // set size of output via handle - it needs to hold the string and an I32 which contains the string length
    // - see update above as DSSetHandleSize(output, message_length+sizeof(int32)); only supports 32-bit systems
    DSSetHandleSize(output, sizeof(LStr) + message_length -1);

    // copy message into resized output buffer
    memcpy((*output)->str,message,message_length);

    // set the value of the string in the output strut
    (*output)->cnt = message_length;
}
```



## Printing to the LabVIEW Debugging Window
`DbgPrintf` is a handy function that allows us to print formatted string and data to LabVIEW's debugging window and can be a lifesaver when debugging tricky issues.

It is defined in the outdated manual [Using External Code in LabVIEW](https://www.ni.com/pdf/manuals/370109b.pdf);

>>>
syntax 
```c
int32 DbgPrintf(CStr cfmt, ..);
```

The first time you call `DbgPrintf`, LabVIEW opens a window to display the text you pass to the function. Subsequent calls to DbgPrintf append new data as new lines in the window. You do not need to pass in the new line character to the function. If you call DbgPrintf with NULL instead of a format string, LabVIEW closes the debugging window. You cannot position or change the size of the window.

The following examples show how to use DbgPrintf.
```c
	DbgPrintf(""); /* print an empty line, opening the window if necessary */

	DbgPrintf("%H", var1); /* print the contents of an LStrHandle (LabVIEW string), opening the window if necessary */

	DbgPrintf(NULL); /* close the debugging window*/
```
>>>

![Debugging Window](<img/debugging-window.png?raw=true>)

# Summary
Hopefully this information has helped you on your journey to integrate shared libraries with LabVIEW and to get up to speed with using VSCode for development and debugging.

Should you want to dive deeper then the following links might guide you;
* The [LabVIEW Wiki](https://labviewwiki.org/wiki/DLL/shared_library) has a page that describes how to write a wrapper around a C++ class so you can create LabVIEW callable functions.
* The [Using External Code in LabVIEW](https://www.ni.com/pdf/manuals/370109b.pdf) manual is a good reference for the LabVIEW C-Integration library function calls even though it is out of date and refers to a "C-Integration Node". Most of it seems transferable but I believe the section _Data Spaces and Code Resources_ is no longer applicable and only the `DS` (Data Space) calls are supported.
* The NI supplied example "External Code (DLL) Execution.vi" (search in example-finder) links to examples of handling different data-types so can be useful when advancing from simple numerics.

Finally - Best of Luck. If you have issues with the code included in this repository please [open an issue](https://gitlab.com/serenial/talks/11-20-labview-and-vscode-shared-libraries/-/issues) and contributions are welcome.