// include LabVIEW Memory manager, File IO and DbgPrintF functions
#include "extcode.h"

// extcode.h is typically located in
// C:\Program Files (x86)\National Instruments\<LabVIEW version>\cintools
// where <LabVIEW version> would be something like LabVIEW 2020

// define a variable for the export directive
#define EXPORT __declspec(dllexport)

// get a messsage from the shared library
EXPORT void get_message(LStrHandle output){

	char message [] = "hello";
    int message_length = strlen(message);

    // LabVIEW Strings are stored as a struct of the form:
    // {
	//        int32	cnt;		/* number of bytes that follow */
	//        uChar	str[1];		/* cnt bytes */
    // } 

    // set size of output via handle - it needs to hold an I32 which contains the string length and the string
    DSSetHandleSize(output, message_length+sizeof(int32));

    // copy message into resized output buffer
    memcpy((*output)->str,message,message_length);

    // set the value of the string in the output strut
    (*output)->cnt = message_length;
}

// write a string to the LabVIEW's debug window
EXPORT void write_message_to_debug(LStrHandle message){
    DbgPrintf("%H", message); // other types of variable also supported by DbgPrintf!
}